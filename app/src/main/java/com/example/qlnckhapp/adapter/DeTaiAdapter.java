package com.example.qlnckhapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.activity.ChiTietDeTaiActivity;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.example.qlnckhapp.model.DeTai;

import java.util.ArrayList;
import java.util.List;

public class DeTaiAdapter extends RecyclerView.Adapter<DeTaiAdapter.DeTaiViewHolder> implements Filterable {
    private Context context;
    private List<DeTai> list;
    private List<DeTai> list2;

    public DeTaiAdapter(Context context, ArrayList<DeTai> list) {
        this.context = context;
        this.list = list;
        this.list2 = list;
    }

    @NonNull
    @Override
    public DeTaiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dong_detai, parent, false);
        return new DeTaiAdapter.DeTaiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeTaiViewHolder holder, int position) {
        DeTai dt = list.get(position);
        Dao_SinhVien daoSV = new Dao_SinhVien(context);
        Dao_DeTai daoDT = new Dao_DeTai(context);


        holder.tenSinhVien.setText(String.valueOf(daoSV.findtenSV(dt.getIdSVDK())));
        holder.tenDT.setText(String.valueOf(dt.getTenDT()));
        holder.moTa.setText(String.valueOf(dt.getMoTa()));
        holder.loaiDT.setText(String.valueOf(daoDT.getLoaiDeTaiById(dt.getLoaiDT())));
        holder.ngaydang.setText(String.valueOf(dt.getNgayDK()));
        holder.xem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChiTietDeTaiActivity.class);
                intent.putExtra("idDT",dt.getIdDT());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String str = charSequence.toString();
                if(str.isEmpty()){
                    list = list2;
                }
                else
                {
                    List<DeTai> d = new ArrayList<>();
                    for(DeTai deTai : list2){
                        if(deTai.getTenDT().toLowerCase().contains(str.toLowerCase())){
                            d.add(deTai);
                        }
                    }
                    list = d;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = list;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (List<DeTai>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class DeTaiViewHolder extends RecyclerView.ViewHolder {

        private TextView tenSinhVien;
        private TextView tenDT;
        private TextView moTa;
        private TextView loaiDT;
        private TextView ngaydang;
        private TextView xem;

        public DeTaiViewHolder(@NonNull View itemView) {
            super(itemView);
            tenSinhVien = (TextView) itemView.findViewById(R.id.texttensv);
            tenDT = (TextView) itemView.findViewById(R.id.txtdetai);
            moTa = (TextView) itemView.findViewById(R.id.textmota);
            loaiDT = (TextView) itemView.findViewById(R.id.txtloaidetai);
            ngaydang = (TextView) itemView.findViewById(R.id.txtdate);
            xem = (TextView) itemView.findViewById(R.id.txtxem);
        }
    }
}

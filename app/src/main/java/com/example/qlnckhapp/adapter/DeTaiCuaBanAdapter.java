package com.example.qlnckhapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.activity.ChiTietDeTaiActivity;
import com.example.qlnckhapp.activity.ChinhSuaDeTaiActivity;
import com.example.qlnckhapp.activity.DeTaiCuaBanActivity;
import com.example.qlnckhapp.activity.QuanLyDeTaiActivity;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.example.qlnckhapp.model.DeTai;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DeTaiCuaBanAdapter extends RecyclerView.Adapter<DeTaiCuaBanAdapter.DeTaiCuaBanViewHolder> implements Filterable {
    private Context context;
    private List<DeTai> list;
    private List<DeTai> list2;
    String id;

    public DeTaiCuaBanAdapter(Context context, ArrayList<DeTai> list) {
        this.context = context;
        this.list = list;
        this.list2 = list;
    }

    @NonNull
    @Override
    public DeTaiCuaBanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dong_detai1, parent, false);
        return new DeTaiCuaBanAdapter.DeTaiCuaBanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeTaiCuaBanViewHolder holder, int position) {
        DeTai dt = list.get(position);
        Dao_SinhVien daoSV = new Dao_SinhVien(context);
        Dao_DeTai daoDT = new Dao_DeTai(context);


        holder.tenSinhVien.setText(String.valueOf(daoSV.findtenSV(dt.getIdSVDK())));
        holder.tenDT.setText(String.valueOf(dt.getTenDT()));
        holder.moTa.setText(String.valueOf(dt.getMoTa()));
        holder.loaiDT.setText(String.valueOf(daoDT.getLoaiDeTaiById(dt.getLoaiDT())));
        holder.ngaydang.setText(String.valueOf(dt.getNgayDK()));
        //nếu là admin mở duyệt
        holder.duyet.setVisibility(View.GONE);
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("masv","");
        if(id.equals("admin"))
        {
            holder.duyet.setVisibility(View.VISIBLE);
        }
        if(daoDT.checkTrangThai(dt.getIdDT())==1)
        {
            holder.duyet.setVisibility(View.GONE);
        }

        holder.xem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChiTietDeTaiActivity.class);
                intent.putExtra("idDT",dt.getIdDT());
                context.startActivity(intent);
            }
        });
        holder.sua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChinhSuaDeTaiActivity.class);
                intent.putExtra("idDT",dt.getIdDT());
                context.startActivity(intent);
            }
        });
        holder.xoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogShown(dt);
            }
        });
        holder.duyet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogShown1(dt);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String str = charSequence.toString();
                if(str.isEmpty()){
                    list = list2;
                }
                else
                {
                    List<DeTai> d = new ArrayList<>();
                    for(DeTai deTai : list2){
                        if(deTai.getTenDT().toLowerCase().contains(str.toLowerCase())){
                            d.add(deTai);
                        }
                    }
                    list = d;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = list;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (List<DeTai>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class DeTaiCuaBanViewHolder extends RecyclerView.ViewHolder {
        private TextView tenSinhVien;
        private TextView tenDT;
        private TextView moTa;
        private TextView loaiDT;
        private TextView ngaydang;
        private TextView xem;
        private ImageButton sua;
        private ImageButton xoa;
        private Button duyet;
        public DeTaiCuaBanViewHolder(@NonNull View itemView) {
            super(itemView);
            tenSinhVien = (TextView) itemView.findViewById(R.id.texttensv);
            tenDT = (TextView) itemView.findViewById(R.id.txtdetai);
            moTa = (TextView) itemView.findViewById(R.id.textmota);
            loaiDT = (TextView) itemView.findViewById(R.id.txtloaidetai);
            ngaydang = (TextView) itemView.findViewById(R.id.txtdate);
            xem = (TextView) itemView.findViewById(R.id.txtxem);
            sua = (ImageButton) itemView.findViewById(R.id.btn_sua);
            xoa = (ImageButton) itemView.findViewById(R.id.btn_xoa);
            duyet = (Button) itemView.findViewById(R.id.duyet);
        }
    }
    void dialogShown(DeTai dt) {
        Dao_SinhVien daoSV = new Dao_SinhVien(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String title = "Bạn có muốn xóa đề tài ?";
        String mess = "Tên đề tài : " + dt.getTenDT()+" \n Người đăng kí: "+ daoSV.findtenSV(dt.getIdSVDK());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(mess);


            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Dao_DeTai daoDeTai = new Dao_DeTai(context);
                daoDeTai.deleteDeTai(dt.getIdDT());
                Toast.makeText(context, "Xóa thành công", Toast.LENGTH_LONG).show();
                if(id.equals("admin"))
                {
                    Intent intent = new Intent(context, QuanLyDeTaiActivity.class);
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, DeTaiCuaBanActivity.class);
                    Dao_SinhVien daoSV = new Dao_SinhVien(context);
                    SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
                    String id = sharedPreferences.getString("masv","");
                    intent.putExtra("idDT",daoSV.findIdSVBymaSV(id));
                    context.startActivity(intent);
                }

            }
        });
        builder.show();
    }
    void dialogShown1(DeTai dt) {
        Dao_SinhVien daoSV = new Dao_SinhVien(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String title = "Bạn có muốn duyệt đề tài ?";
        String mess = "Tên đề tài : " + dt.getTenDT()+" \n Người đăng kí: "+ daoSV.findtenSV(dt.getIdSVDK());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(mess);


        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Dao_DeTai daoDeTai = new Dao_DeTai(context);
                if(daoDeTai.duyetDeTai(dt.getIdDT())){
                    Toast.makeText(context, "Duyệt thành công", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, QuanLyDeTaiActivity.class);
                    context.startActivity(intent);
                }
                else
                {
                    Toast.makeText(context, "Duyệt thất bại", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
        builder.show();
    }
}

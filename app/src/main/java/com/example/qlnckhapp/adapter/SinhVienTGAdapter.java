package com.example.qlnckhapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.model.SinhVienTG;

import java.util.ArrayList;
import java.util.List;

public class SinhVienTGAdapter extends RecyclerView.Adapter<SinhVienTGAdapter.SinhVienTGViewHolder>{
    private Context context;
    private List<SinhVienTG> list;

    public SinhVienTGAdapter(Context context, ArrayList<SinhVienTG> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SinhVienTGViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sinhvienthamgia, parent, false);
        return new SinhVienTGViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SinhVienTGViewHolder holder, int position) {
        SinhVienTG svTg = list.get(position);

        holder.maSinhVien.setText(String.valueOf(svTg.getMaSV()));
        holder.tenSinhVien.setText(String.valueOf(svTg.getTenSV()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SinhVienTGViewHolder extends RecyclerView.ViewHolder {
        private TextView maSinhVien;
        private TextView tenSinhVien;
        public SinhVienTGViewHolder(@NonNull View itemView) {
            super(itemView);
            maSinhVien = (TextView) itemView.findViewById(R.id.tv_masv);
            tenSinhVien = (TextView) itemView.findViewById(R.id.tv_tensv);
        }
    }
}

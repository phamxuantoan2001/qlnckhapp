package com.example.qlnckhapp.model;

public class LoaiSP {
    Integer idSP;
    String tenSP;

    public LoaiSP() {
    }

    public LoaiSP(Integer idSP, String tenSP) {
        this.idSP = idSP;
        this.tenSP = tenSP;
    }

    public Integer getIdSP() {
        return idSP;
    }

    public void setIdSP(Integer idSP) {
        this.idSP = idSP;
    }

    public String getTenSP() {
        return tenSP;
    }

    public void setTenSP(String tenSP) {
        this.tenSP = tenSP;
    }
}

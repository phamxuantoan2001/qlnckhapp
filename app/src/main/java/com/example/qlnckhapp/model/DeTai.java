package com.example.qlnckhapp.model;

import com.example.qlnckhapp.dao.Dao_SinhVien;

public class DeTai {
    Integer idDT;
    Integer idSVDK;
    String trangthai;
    String tenDT;
    String moTa;
    String mucTieu;
    Integer soluongTG;
    String giaoVienHD;
    Integer loaiDT;
    Integer idLinhVuc;
    String phuongphapNC;
    Integer loaiSP;
    String kinhphiDK;
    String ngayDK;

    public DeTai() {
    }

    public DeTai(Integer idDT, Integer idSVDK, String trangthai, String tenDT, String moTa, String mucTieu, Integer soluongTG, String giaoVienHD, Integer loaiDT, Integer idLinhVuc, String phuongphapNC, Integer loaiSP, String kinhphiDK, String ngayDK) {
        this.idDT = idDT;
        this.idSVDK = idSVDK;
        this.trangthai = trangthai;
        this.tenDT = tenDT;
        this.moTa = moTa;
        this.mucTieu = mucTieu;
        this.soluongTG = soluongTG;
        this.giaoVienHD = giaoVienHD;
        this.loaiDT = loaiDT;
        this.idLinhVuc = idLinhVuc;
        this.phuongphapNC = phuongphapNC;
        this.loaiSP = loaiSP;
        this.kinhphiDK = kinhphiDK;
        this.ngayDK = ngayDK;
    }

    public DeTai(Integer idDT, Integer idSVDK, String tenDT, String moTa, Integer loaiDT, String ngayDK) {
        this.idDT = idDT;
        this.idSVDK = idSVDK;
        this.tenDT = tenDT;
        this.moTa = moTa;
        this.loaiDT = loaiDT;
        this.ngayDK = ngayDK;
    }

    public Integer getIdDT() {
        return idDT;
    }

    public void setIdDT(Integer idDT) {
        this.idDT = idDT;
    }

    public Integer getIdSVDK() {
        return idSVDK;
    }

    public void setIdSVDK(Integer idSVDK) {
        this.idSVDK = idSVDK;
    }

    public String getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(String trangthai) {
        this.trangthai = trangthai;
    }

    public String getTenDT() {
        return tenDT;
    }

    public void setTenDT(String tenDT) {
        this.tenDT = tenDT;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMucTieu() {
        return mucTieu;
    }

    public void setMucTieu(String mucTieu) {
        this.mucTieu = mucTieu;
    }

    public Integer getSoluongTG() {
        return soluongTG;
    }

    public void setSoluongTG(Integer soluongTG) {
        this.soluongTG = soluongTG;
    }

    public String getGiaoVienHD() {
        return giaoVienHD;
    }

    public void setGiaoVienHD(String giaoVienHD) {
        this.giaoVienHD = giaoVienHD;
    }

    public Integer getLoaiDT() {
        return loaiDT;
    }

    public void setLoaiDT(Integer loaiDT) {
        this.loaiDT = loaiDT;
    }

    public Integer getIdLinhVuc() {
        return idLinhVuc;
    }

    public void setIdLinhVuc(Integer idLinhVuc) {
        this.idLinhVuc = idLinhVuc;
    }

    public String getPhuongphapNC() {
        return phuongphapNC;
    }

    public void setPhuongphapNC(String phuongphapNC) {
        this.phuongphapNC = phuongphapNC;
    }

    public Integer getLoaiSP() {
        return loaiSP;
    }

    public void setLoaiSP(Integer loaiSP) {
        this.loaiSP = loaiSP;
    }

    public String getKinhphiDK() {
        return kinhphiDK;
    }

    public void setKinhphiDK(String kinhphiDK) {
        this.kinhphiDK = kinhphiDK;
    }

    public String getNgayDK() {
        return ngayDK;
    }

    public void setNgayDK(String ngayDK) {
        this.ngayDK = ngayDK;
    }
}

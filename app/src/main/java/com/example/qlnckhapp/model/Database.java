package com.example.qlnckhapp.model;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Database {
    String DATABASE_NAME = "detaiNCKH.sqlite";
    String DB_PATH_SUFFIX = "/databases/";


    public Database() {
    }

    public String getDATABASE_NAME() {
        return DATABASE_NAME;
    }

    public void setDATABASE_NAME(String DATABASE_NAME) {
        this.DATABASE_NAME = DATABASE_NAME;
    }

    public String getDB_PATH_SUFFIX() {
        return DB_PATH_SUFFIX;
    }

    public void setDB_PATH_SUFFIX(String DB_PATH_SUFFIX) {
        this.DB_PATH_SUFFIX = DB_PATH_SUFFIX;
    }
}

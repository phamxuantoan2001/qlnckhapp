package com.example.qlnckhapp.model;

public class SinhVien {
    int idSV;
    String tenSV;
    String maSV;
    String email;
    String lopSH;
    String sdt;
    String gioiTinh;
    String ngaySinh;
    String matkhau;
    String hinhanh;

    public SinhVien() {
    }

    public SinhVien(int idSV, String tenSV, String maSV, String email,
                    String lopSH, String sdt, String gioiTinh, String ngaySinh, String matkhau, String hinhanh) {
        this.idSV = idSV;
        this.tenSV = tenSV;
        this.maSV = maSV;
        this.email = email;
        this.lopSH = lopSH;
        this.sdt = sdt;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.matkhau = matkhau;
        this.hinhanh = hinhanh;
    }

    public int getIdSV() {
        return idSV;
    }

    public void setIdSV(int idSV) {
        this.idSV = idSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLopSH() {
        return lopSH;
    }

    public void setLopSH(String lopSH) {
        this.lopSH = lopSH;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getMatkhau() {
        return matkhau;
    }

    public void setMatkhau(String matkhau) {
        this.matkhau = matkhau;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }
}

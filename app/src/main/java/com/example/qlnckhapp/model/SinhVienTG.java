package com.example.qlnckhapp.model;

public class SinhVienTG {
    Integer idNTG;
    Integer idDT;
    String maSV;
    String tenSV;

    public SinhVienTG() {
    }

    public SinhVienTG(Integer idNTG, Integer idDT, String maSV, String tenSV) {
        this.idNTG = idNTG;
        this.idDT = idDT;
        this.maSV = maSV;
        this.tenSV = tenSV;
    }

    public Integer getIdNTG() {
        return idNTG;
    }

    public void setIdNTG(Integer idNTG) {
        this.idNTG = idNTG;
    }

    public Integer getIdDT() {
        return idDT;
    }

    public void setIdDT(Integer idDT) {
        this.idDT = idDT;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }
}

package com.example.qlnckhapp.model;

public class LoaiDeTai {
    Integer idLDT;
    String tenLoai;

    public LoaiDeTai() {
    }

    public LoaiDeTai(Integer idLDT, String tenLoai) {
        this.idLDT = idLDT;
        this.tenLoai = tenLoai;
    }

    public Integer getIdLDT() {
        return idLDT;
    }

    public void setIdLDT(Integer idLDT) {
        this.idLDT = idLDT;
    }

    public String getTenLoai() {
        return tenLoai;
    }

    public void setTenLoai(String tenLoai) {
        this.tenLoai = tenLoai;
    }
}

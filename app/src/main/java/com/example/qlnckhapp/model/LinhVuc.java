package com.example.qlnckhapp.model;

public class LinhVuc {
    Integer idLV;
    String tenLV;

    public LinhVuc() {
    }

    public LinhVuc(Integer idLV, String tenLV) {
        this.idLV = idLV;
        this.tenLV = tenLV;
    }

    public Integer getIdLV() {
        return idLV;
    }

    public void setIdLV(Integer idLV) {
        this.idLV = idLV;
    }

    public String getTenLV() {
        return tenLV;
    }

    public void setTenLV(String tenLV) {
        this.tenLV = tenLV;
    }
}

package com.example.qlnckhapp.dao;




import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;


import com.example.qlnckhapp.model.Database;

public class Dao_SinhVien {

    Database sql = new Database();
    SQLiteDatabase database;

    public Dao_SinhVien(Context context)
    {
        database = context.openOrCreateDatabase(sql.getDATABASE_NAME(), context.MODE_PRIVATE, null);
    }


    public boolean dangNhap(String inputMasv, String inputMatkhau)
    {
        Integer id = 0;
        String q = "SELECT * FROM SinhVien WHERE maSV = ? AND matkhau = ?";
        Cursor cursor = database.rawQuery(q, new String[]{inputMasv,inputMatkhau});
        while (cursor.moveToNext())
        {
            id = cursor.getInt(0);
        }
        cursor.close();
        if (id != 0)
            return true;
        else
            return false;
    }
    public String findtenSV(Integer id){
        String n = id.toString();
        String tenSV = null;
        String q = "SELECT tenSV FROM SinhVien WHERE idSV = ?";
        Cursor cursor = database.rawQuery(q, new String[]{String.valueOf(id)});
        while (cursor.moveToNext())
        {
            String ten=cursor.getString(0);
            tenSV = ten;
        }
        cursor.close();
        return tenSV;
    }
    public Integer findIdSVBymaSV(String ma){
        Integer idSV = null;
        String q = "SELECT idSV FROM SinhVien WHERE maSV = ?";
        Cursor cursor = database.rawQuery(q, new String[]{ma});
        while (cursor.moveToNext())
        {
            Integer ten=cursor.getInt(0);
            idSV = ten;
        }
        cursor.close();
        return idSV;
    }
    public String findtenSVByMSV(String id){

        String tenSV = null;
        String q = "SELECT tenSV FROM SinhVien WHERE maSV = ?";
        Cursor cursor = database.rawQuery(q, new String[]{String.valueOf(id)});
        while (cursor.moveToNext())
        {
            String ten=cursor.getString(0);
            tenSV = ten;
        }
        cursor.close();
        return tenSV;
    }

}

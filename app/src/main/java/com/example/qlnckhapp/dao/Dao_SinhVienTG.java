package com.example.qlnckhapp.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.qlnckhapp.model.Database;
import com.example.qlnckhapp.model.DeTai;
import com.example.qlnckhapp.model.SinhVienTG;

import java.util.ArrayList;

public class Dao_SinhVienTG {
    Database sql = new Database();
    SQLiteDatabase database;

    public Dao_SinhVienTG(Context context)
    {
        database = context.openOrCreateDatabase(sql.getDATABASE_NAME(), context.MODE_PRIVATE, null);
    }
    public ArrayList<SinhVienTG> getAllSinhVienSub(Integer id){
        ArrayList<SinhVienTG> list = new ArrayList<>();
        String q = "SELECT * FROM SinhVienTG WHERE idDT = ?";
        Cursor cursor = database.rawQuery(q, new String[]{id.toString()});
        while (cursor.moveToNext())
        {
            Integer idNTG=cursor.getInt(0);
            Integer idDT=cursor.getInt(1);
            String masv=cursor.getString(2);
            String tensv=cursor.getString(3);
            SinhVienTG sv = new SinhVienTG(idNTG,idDT,masv,tensv);
            list.add(sv);
        }
        cursor.close();

        return list;
    }
    public boolean createSVTG(SinhVienTG sv)
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("idDT",sv.getIdDT());
        contentValues.put("maSV",sv.getMaSV());
        contentValues.put("tenSV",sv.getTenSV());

        //Thêm đề tài mới vào database
        if(database.insert("SinhVienTG", null, contentValues) == -1)
        {
            return  false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateSVTG(SinhVienTG sv) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("idDT",sv.getIdDT());
        contentValues.put("maSV",sv.getMaSV());
        contentValues.put("tenSV",sv.getTenSV());
        //Sửa đề tài vào database
        int ret = database.update("SinhVienTG",contentValues,
                "idNTG = ? ",new String[]{String.valueOf(sv.getIdNTG())});
        if(ret == 0)
            return  false;
        else
            return true;
    }
    public Integer findIdNTG(Integer iddt,String masv)
    {
        Integer idSV = null;
        String q = "SELECT idNTG FROM SinhVienTG WHERE idDT = ? and maSV = ?";
        Cursor cursor = database.rawQuery(q, new String[]{String.valueOf(iddt),masv});
        while (cursor.moveToNext())
        {
            Integer ID=cursor.getInt(0);
            idSV = ID;
        }
        cursor.close();
        return idSV;
    }
}

package com.example.qlnckhapp.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.qlnckhapp.model.Database;
import com.example.qlnckhapp.model.DeTai;
import com.example.qlnckhapp.model.LinhVuc;
import com.example.qlnckhapp.model.LoaiDeTai;
import com.example.qlnckhapp.model.LoaiSP;

import java.util.ArrayList;
import java.util.List;

public class Dao_DeTai {
    Database sql = new Database();
    SQLiteDatabase database;

    public Dao_DeTai(Context context)
    {
        database = context.openOrCreateDatabase(sql.getDATABASE_NAME(), context.MODE_PRIVATE, null);
    }
    public ArrayList<DeTai> getAllDeTaiSub(){
        ArrayList<DeTai> arrayDT = new ArrayList<>();
        String q = "SELECT id,idSVDK,tenDT,moTa,loaiDT,ngayDK FROM DeTai WHERE trangthai=1 ORDER BY ngayDK DESC";
        Cursor cursor = database.rawQuery(q, null);
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            Integer masv=cursor.getInt(1);
            String tendt=cursor.getString(2);
            String mota=cursor.getString(3);
            Integer loaiDT= cursor.getInt(4);
            String ngaydang = cursor.getString(5);
            DeTai dt = new DeTai(id,masv,tendt,mota,loaiDT,ngaydang);
            arrayDT.add(dt);
        }
        cursor.close();
        return arrayDT;
    }
    public Integer checkTrangThai(Integer id)
    {
        int t=0;
        String q = "SELECT trangthai From DeTai WHERE id= ?";
        Cursor cursor = database.rawQuery(q, new String[]{id.toString()});
        while (cursor.moveToNext())
        {
            Integer check =cursor.getInt(0);
            t = check;
        }
        cursor.close();
        return t;
    }
    public ArrayList<DeTai> getAllDeTai(){
        ArrayList<DeTai> arrayDT = new ArrayList<>();
        String q = "SELECT id,idSVDK,tenDT,moTa,loaiDT,ngayDK FROM DeTai ORDER BY ngayDK DESC";
        Cursor cursor = database.rawQuery(q, null);
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            Integer masv=cursor.getInt(1);
            String tendt=cursor.getString(2);
            String mota=cursor.getString(3);
            Integer loaiDT= cursor.getInt(4);
            String ngaydang = cursor.getString(5);
            DeTai dt = new DeTai(id,masv,tendt,mota,loaiDT,ngaydang);
            arrayDT.add(dt);
        }
        cursor.close();
        return arrayDT;
    }
    public ArrayList<DeTai> getAllDeTaiSubBySinhVien(Integer idsv){
        ArrayList<DeTai> arrayDT = new ArrayList<>();
        String q = "SELECT id,idSVDK,tenDT,moTa,loaiDT,ngayDK FROM DeTai WHERE idSVDK = ? ORDER BY ngayDK DESC";
        Cursor cursor = database.rawQuery(q, new String[]{String.valueOf(idsv)});
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            Integer idsvdk=cursor.getInt(1);
            String tendt=cursor.getString(2);
            String mota=cursor.getString(3);
            Integer loaiDT= cursor.getInt(4);
            String ngaydang = cursor.getString(5);
            DeTai dt = new DeTai(id,idsvdk,tendt,mota,loaiDT,ngaydang);
            arrayDT.add(dt);
        }
        cursor.close();
        return arrayDT;
    }
    public DeTai getDeTaiById(Integer id1){
        DeTai dt=null;
        String q = "SELECT * FROM DeTai WHERE id = ?";
        Cursor cursor = database.rawQuery(q, new String[]{String.valueOf(id1)});
        while (cursor.moveToNext())
        {
            Integer id = cursor.getInt(0);
            Integer idsv=cursor.getInt(1);
            Integer trangthai=cursor.getInt(2);
            String tendt=cursor.getString(3);
            String mota=cursor.getString(4);
            String muctieu=cursor.getString(5);
            Integer soluong=cursor.getInt(6);
            String giaovien=cursor.getString(7);
            Integer loaiDT= cursor.getInt(8);
            Integer loaiLV= cursor.getInt(9);
            String phuongphap=cursor.getString(10);
            Integer loaiSP= cursor.getInt(11);
            String kinhphi=cursor.getString(12);
            String ngaydang = cursor.getString(13);
            dt = new DeTai(id, idsv, String.valueOf(trangthai), tendt,
                    mota, muctieu, soluong, giaovien,
                    loaiDT, loaiLV, phuongphap, loaiSP,
                    String.valueOf(kinhphi), ngaydang);
        }
        cursor.close();

        return dt;
    }
    public String getLoaiLinhVucById(Integer id)
    {
        String tenlv = null;
        String q = "SELECT * FROM LinhVuc WHERE idLV =  ? ";
        Cursor cursor = database.rawQuery(q, new String[]{id.toString()});
        while (cursor.moveToNext())
        {
            Integer id1 =cursor.getInt(0);
            String ten=cursor.getString(1);
            tenlv = ten;
        }
        cursor.close();
        return tenlv;
    }
    public String getLoaiDeTaiById(Integer id)
    {
        String loaidt = null;
        String q = "SELECT * FROM LoaiDeTai WHERE idLDT =  ? ";
        Cursor cursor = database.rawQuery(q, new String[]{id.toString()});
        while (cursor.moveToNext())
        {
            Integer id1 =cursor.getInt(0);
            String ten=cursor.getString(1);
            loaidt = ten;
        }
        cursor.close();
        return loaidt;
    }
    public String getLoaiSanPhamById(Integer id)
    {
        String loaisp = null;
        String q = "SELECT * FROM LoaiSP WHERE idSP =  ? ";
        Cursor cursor = database.rawQuery(q, new String[]{id.toString()});
        while (cursor.moveToNext())
        {
            Integer id1 =cursor.getInt(0);
            String ten=cursor.getString(1);
            loaisp = ten;
        }
        cursor.close();
        return loaisp;
    }
    public boolean createDeTai(DeTai deTai)
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("idSVDK",deTai.getIdSVDK());
        contentValues.put("trangthai",deTai.getTrangthai());
        contentValues.put("tenDT",deTai.getTenDT());
        contentValues.put("moTa",deTai.getMoTa());
        contentValues.put("mucTieu",deTai.getMucTieu());
        contentValues.put("soluongTG",deTai.getSoluongTG());
        contentValues.put("giaovienHD",deTai.getGiaoVienHD());
        contentValues.put("loaiDT",deTai.getLoaiDT());
        contentValues.put("loaiLV",deTai.getIdLinhVuc());
        contentValues.put("phuongphapNC",deTai.getPhuongphapNC());
        contentValues.put("loaiSP",deTai.getLoaiSP());
        contentValues.put("kinhPhiDK",deTai.getKinhphiDK());
        contentValues.put("ngayDK",deTai.getNgayDK());
        //Thêm đề tài mới vào database
        if(database.insert("DeTai", null, contentValues) == -1)
        {
            return  false;
        }
        else
        {
            return true;
        }
    }
    public boolean updateDeTai(DeTai deTai)
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("idSVDK",deTai.getIdSVDK());
        contentValues.put("trangthai",deTai.getTrangthai());
        contentValues.put("tenDT",deTai.getTenDT());
        contentValues.put("moTa",deTai.getMoTa());
        contentValues.put("mucTieu",deTai.getMucTieu());
        contentValues.put("soluongTG",deTai.getSoluongTG());
        contentValues.put("giaovienHD",deTai.getGiaoVienHD());
        contentValues.put("loaiDT",deTai.getLoaiDT());
        contentValues.put("loaiLV",deTai.getIdLinhVuc());
        contentValues.put("phuongphapNC",deTai.getPhuongphapNC());
        contentValues.put("loaiSP",deTai.getLoaiSP());
        contentValues.put("kinhPhiDK",deTai.getKinhphiDK());
        contentValues.put("ngayDK",deTai.getNgayDK());
        //Sửa đề tài vào database
        int ret = database.update("DeTai",contentValues,
                "id = ?",new String[]{String.valueOf(deTai.getIdDT())});
        if(ret == 0)
            return  false;
        else
            return true;
    }
    public void deleteDeTai(Integer id)
    {

        //Xóa 1  đề tài database
        int ret = database.delete("DeTai",
                "id = ?",new String[]{String.valueOf(id)});
    }
    public void deleteAllDeTai(Integer id)
    {

        //Xóa tất cả đề tài database
        int ret = database.delete("DeTai", null,null);
    }
    //duyetdetai
    public boolean duyetDeTai(Integer id)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put("trangthai",1);
        //Sửa đề tài vào database
        int ret = database.update("DeTai",contentValues,
                "id = ?",new String[]{String.valueOf(id)});
        if(ret == 0)
            return  false;
        else
            return true;
    }



    public int layIdmoi(){
        ArrayList<DeTai> array = new ArrayList<>();
        array = getAllDeTaiSub();
        return array.size();
    }
    public List<String> getAllldt(){
        List<String> array = new ArrayList<>();
        String q = "SELECT * FROM LoaiDeTai";
        Cursor cursor = database.rawQuery(q, null);
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            String tenldt=cursor.getString(1);
            array.add(tenldt);
        }
        cursor.close();
        return array;
    }
    public List<String> getAllLV(){
        List<String> array = new ArrayList<>();
        String q = "SELECT * FROM LinhVuc";
        Cursor cursor = database.rawQuery(q, null);
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            String ten=cursor.getString(1);
            array.add(ten);
        }
        cursor.close();
        return array;
    }
    public List<String> getAlllsp(){
        List<String> array = new ArrayList<>();
        String q = "SELECT * FROM LoaiSP";
        Cursor cursor = database.rawQuery(q, null);
        while (cursor.moveToNext())
        {
            Integer id=cursor.getInt(0);
            String tensp=cursor.getString(1);
            array.add(tensp);
        }
        cursor.close();
        return array;
    }
    public Integer getIDLDT(String ten){
        int id=0;
        String q = "SELECT * FROM LoaiDeTai WHERE tenLoai = ?";
        Cursor cursor = database.rawQuery(q, new String[]{ten});
        while (cursor.moveToNext())
        {
            Integer cid=cursor.getInt(0);
            id = cid;
        }
        cursor.close();
        return id;
    }
    public Integer getIDLV(String ten){
        int id=0;
        String q = "SELECT * FROM LinhVuc WHERE tenLV = ?";
        Cursor cursor = database.rawQuery(q, new String[]{ten});
        while (cursor.moveToNext())
        {
            Integer cid=cursor.getInt(0);
            id = cid;
        }
        cursor.close();
        return id;
    }
    public Integer getIDSP(String ten){
        int id=0;
        String q = "SELECT * FROM LoaiSP WHERE tenSP = ?";
        Cursor cursor = database.rawQuery(q, new String[]{ten});
        while (cursor.moveToNext())
        {
            Integer cid=cursor.getInt(0);
            id = cid;
        }
        cursor.close();
        return id;
    }
}

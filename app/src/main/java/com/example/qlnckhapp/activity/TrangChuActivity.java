package com.example.qlnckhapp.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.adapter.DeTaiAdapter;
import com.example.qlnckhapp.adapter.SinhVienTGAdapter;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.example.qlnckhapp.model.DeTai;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.util.ArrayList;

public class TrangChuActivity extends AppCompatActivity {
    RecyclerView lviewDT;
    ArrayList<DeTai> arrayDT = null;
    DeTaiAdapter adapter;
    private SearchView searchView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chu);
        BottomNavigationView bt  = findViewById(R.id.bottomnavi);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        bt.setSelectedItemId(R.id.home);
        bt.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.home:

                        break;
                    case R.id.profile:
                        Intent home = new Intent(TrangChuActivity.this,ThongTinNguoiDungActivity.class);
                        startActivity(home);
                        break;
                }
                return true;
            }
        });

        lviewDT = findViewById(R.id.lv1);
        getAllDeTai();


    }

    private void getAllDeTai(){
        Dao_DeTai daoDT = new Dao_DeTai(this);
        arrayDT = new ArrayList<>();
        arrayDT = daoDT.getAllDeTaiSub();

        adapter = new DeTaiAdapter(TrangChuActivity.this,arrayDT);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TrangChuActivity.this, LinearLayoutManager.VERTICAL, false);
        lviewDT.setLayoutManager(linearLayoutManager);
        lviewDT.setAdapter(adapter);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.seach_detai,menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);

                return false;
            }
        });

        return true;
    }
}
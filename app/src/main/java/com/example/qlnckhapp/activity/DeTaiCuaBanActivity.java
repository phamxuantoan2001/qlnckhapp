package com.example.qlnckhapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.adapter.DeTaiAdapter;
import com.example.qlnckhapp.adapter.DeTaiCuaBanAdapter;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.model.DeTai;

import java.util.ArrayList;

public class DeTaiCuaBanActivity extends AppCompatActivity {

    RecyclerView lviewDT;
    ArrayList<DeTai> arrayDT = null;
    DeTaiCuaBanAdapter adapter;
    Integer idDT;
    ImageButton btn_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_de_tai_cua_ban);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent = getIntent();
        idDT = intent.getIntExtra("idDT",0);
        lviewDT = findViewById(R.id.recyle_detaicuaban);
        getAllDeTai();
    }

    private void getAllDeTai(){
        Dao_DeTai daoDT = new Dao_DeTai(this);
        arrayDT = new ArrayList<>();
        arrayDT = daoDT.getAllDeTaiSubBySinhVien(idDT);

        adapter = new DeTaiCuaBanAdapter(DeTaiCuaBanActivity.this,arrayDT);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DeTaiCuaBanActivity.this, LinearLayoutManager.VERTICAL, false);
        lviewDT.setLayoutManager(linearLayoutManager);
        lviewDT.setAdapter(adapter);
    }
}
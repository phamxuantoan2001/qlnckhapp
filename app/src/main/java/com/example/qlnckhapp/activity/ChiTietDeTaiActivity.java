package com.example.qlnckhapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.adapter.SinhVienTGAdapter;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.dao.Dao_SinhVienTG;
import com.example.qlnckhapp.model.DeTai;
import com.example.qlnckhapp.model.SinhVienTG;

import java.util.ArrayList;

public class ChiTietDeTaiActivity extends AppCompatActivity {
    TextView tvTenDeTai,tvMoTa,tvMucTieu,tvGiaoVien,tvLoaiDeTai,tvLinhVuc,tvPhuongPhap,tvLoaiSP,tvKinhPhi,tvNgayDangKi;
    RecyclerView lvSVThamGia;
    DeTai detai;
    SinhVienTGAdapter svTgAdapter;
    ArrayList<SinhVienTG> list;
    Integer idDT;
    ImageButton btn_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_de_tai);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        idDT = intent.getIntExtra("idDT",0);

        tvTenDeTai = (TextView) findViewById(R.id.tv_tendetai);
        tvMoTa = (TextView) findViewById(R.id.tv_mota);
        tvMucTieu = (TextView) findViewById(R.id.tv_muctieu);
        tvGiaoVien = (TextView) findViewById(R.id.tv_giaovienhuongdan);
        tvLoaiDeTai = (TextView) findViewById(R.id.tv_loaidetai);
        tvLinhVuc = (TextView) findViewById(R.id.tv_linhvuc);
        tvPhuongPhap = (TextView) findViewById(R.id.tv_phuongphap);
        tvLoaiSP = (TextView) findViewById(R.id.tv_loaisanpham);
        tvKinhPhi = (TextView) findViewById(R.id.tv_kinhphi);
        tvNgayDangKi = (TextView) findViewById(R.id.tv_ngaydangki);
        lvSVThamGia =(RecyclerView) findViewById(R.id.lv_sinhvienthamgia);


        btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        getDeTai(detai);
        setDeTaiToView(detai);

    }
    void setDeTaiToView(DeTai d){
       Dao_DeTai daoDT = new Dao_DeTai(this);
        tvTenDeTai.setText(d.getTenDT());
        tvMoTa.setText(d.getMoTa());
        if(d.getMucTieu().isEmpty())
        {
            tvMucTieu.setText("Chưa có");
        }
        else
        {
            tvMucTieu.setText(d.getMucTieu());
        }
        tvGiaoVien.setText(d.getGiaoVienHD());
        tvLoaiDeTai.setText(daoDT.getLoaiDeTaiById(d.getLoaiDT()));
        tvLinhVuc.setText(daoDT.getLoaiLinhVucById(d.getIdLinhVuc()));
        tvPhuongPhap.setText(d.getPhuongphapNC());
        tvLoaiSP.setText(daoDT.getLoaiSanPhamById(d.getLoaiSP()));
        tvKinhPhi.setText(d.getKinhphiDK() + "  VNĐ");
        tvNgayDangKi.setText(d.getNgayDK());

        svTgAdapter = new SinhVienTGAdapter(ChiTietDeTaiActivity.this,list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChiTietDeTaiActivity.this, LinearLayoutManager.VERTICAL, false);
        lvSVThamGia.setLayoutManager(linearLayoutManager);
        lvSVThamGia.setAdapter(svTgAdapter);
    }
    void getDeTai(DeTai deTai)
    {
        Dao_DeTai daoDT = new Dao_DeTai(this);
        detai = new DeTai();
        detai = daoDT.getDeTaiById(idDT);

        Dao_SinhVienTG dao_SVTG = new Dao_SinhVienTG(this);
        list = new ArrayList<>();
        list = dao_SVTG.getAllSinhVienSub(idDT);
    }
}
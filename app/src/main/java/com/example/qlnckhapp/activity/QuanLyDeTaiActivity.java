package com.example.qlnckhapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.adapter.DeTaiCuaBanAdapter;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.model.DeTai;

import java.util.ArrayList;

public class QuanLyDeTaiActivity extends AppCompatActivity {
    RecyclerView lviewDT;
    ArrayList<DeTai> arrayDT = null;
    DeTaiCuaBanAdapter adapter;
    Integer idDT;
    ImageButton btn_return;
    private SearchView searchView;
    RelativeLayout re;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_de_tai);
        getSupportActionBar().setTitle("Quản lý đề tài");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        re = (RelativeLayout) findViewById(R.id.re1);
        re.setVisibility(View.GONE);
        btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent = getIntent();
        idDT = intent.getIntExtra("idDT",0);
        lviewDT = findViewById(R.id.recyle_quanly);
        getAllDeTai();
    }
    private void getAllDeTai(){
        Dao_DeTai daoDT = new Dao_DeTai(this);
        arrayDT = new ArrayList<>();
        arrayDT = daoDT.getAllDeTai();

        adapter = new DeTaiCuaBanAdapter(QuanLyDeTaiActivity.this,arrayDT);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(QuanLyDeTaiActivity.this, LinearLayoutManager.VERTICAL, false);
        lviewDT.setLayoutManager(linearLayoutManager);
        lviewDT.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.seach_detai,menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);

                return false;
            }
        });

        return true;
    }
}
package com.example.qlnckhapp.activity;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.example.qlnckhapp.model.Database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DangNhapActivity extends AppCompatActivity {
    Database sql = new Database();
    Button btnLogin;
    String DATABASE_NAME = sql.getDATABASE_NAME();
    String DB_PATH_SUFFIX = sql.getDB_PATH_SUFFIX();
    SQLiteDatabase database = null;
    EditText edMaSV,edMatKhau;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btnLogin = (Button) findViewById(R.id.btn_login);
        edMaSV = (EditText) findViewById(R.id.ed_name);
        edMatKhau = (EditText) findViewById(R.id.ed_password);

        SaochepCSDLvaomay();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dangNhap();
            }
        });
    }

    private void dangNhap() {
        Dao_SinhVien dao = new Dao_SinhVien(this);
        String masv = edMaSV.getText().toString();
        String pass = edMatKhau.getText().toString();
        if(masv.equals("") || pass.equals("")){
            Toast.makeText(this, "Chưa nhập tên đăng nhập hoặc mật khẩu", Toast.LENGTH_LONG).show();
        }
        else {
            if(dao.dangNhap(masv,pass)){
                String tensv = dao.findtenSVByMSV(masv);
                SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("masv", masv);
                editor.putString("tensv",tensv);
                editor.commit();
                Intent intent = new Intent(DangNhapActivity.this,TrangChuActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(this, "Đăng nhập thành công", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "Đăng nhập thất bại", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void SaochepCSDLvaomay() {
        File dbFile = getDatabasePath(DATABASE_NAME);
        if (!dbFile.exists()) {
            try {
                CopyDataBaseFromAsset();
                Toast.makeText(this, "Dữ liệu đã sẵn sàng", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(this, "Dữ liệu sao chép lỗi"+e.toString(), Toast.LENGTH_LONG).show();
                Log.d("Loi1",e.toString());
            }
        }
    }

    private String LayDuongDan() {
        return getApplicationInfo().dataDir + DB_PATH_SUFFIX+ DATABASE_NAME;
    }
    public void CopyDataBaseFromAsset()
    {
        try {
            InputStream myInput;
            myInput = getAssets().open(DATABASE_NAME);
            // Path to the just created empty db
            String outFileName = LayDuongDan();
            // if the path doesn't exist first, create it
            File f = new File(getApplicationInfo().dataDir + DB_PATH_SUFFIX);
            if (!f.exists())
                f.mkdir();
            // Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
                // Close the streams
                myOutput.flush();
                myOutput.close();
                myInput.close();
        }catch(IOException e){
            e.printStackTrace();
            Log.d("Loi2",e.toString());
        }
    }
}
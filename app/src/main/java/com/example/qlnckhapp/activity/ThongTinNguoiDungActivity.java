package com.example.qlnckhapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class ThongTinNguoiDungActivity extends AppCompatActivity {
    Button btndetai,btndkidetai,btndangxuat,btnql;
    TextView ten;
    String id;
    String ten1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_thong_tin_nguoi_dung);
        BottomNavigationView bt  = findViewById(R.id.bottomnavi);
        ten = (TextView) findViewById(R.id.tenSV);
        btnql = (Button) findViewById(R.id.btn_quanly);
        btndetai =findViewById(R.id.btn_detai);
        btnql.setVisibility(View.GONE);
        //
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("masv","");
        ten1 = sharedPreferences.getString("tensv","");
        ten.setText(ten1);
        if(id.equals("admin"))
        {
            btnql.setVisibility(View.VISIBLE);
            btndetai.setVisibility(View.GONE);
        }
        btnql.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThongTinNguoiDungActivity.this, QuanLyDeTaiActivity.class);
                startActivity(intent);
                finish();
            }
        });



        bt.setSelectedItemId(R.id.profile);
        bt.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.home:
                        Intent home = new Intent(ThongTinNguoiDungActivity.this,TrangChuActivity.class);
                        startActivity(home);
                        break;
                    case R.id.profile:

                        break;
                }
                return true;
            }
        });


        Dao_SinhVien daoSV = new Dao_SinhVien(this);
        btndetai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThongTinNguoiDungActivity.this, DeTaiCuaBanActivity.class);
                intent.putExtra("idDT",daoSV.findIdSVBymaSV(id));
                startActivity(intent);
                finish();
            }
        });
        btndkidetai =findViewById(R.id.btn_dkdetai);
        btndkidetai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThongTinNguoiDungActivity.this, DangKiDeTaiActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btndangxuat =findViewById(R.id.btn_dangxuat);
        btndangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThongTinNguoiDungActivity.this, DangNhapActivity.class);
                SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("masv", "");
                editor.putString("tensv", "");
                editor.commit();
                startActivity(intent);
            }
        });
    }
}
package com.example.qlnckhapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.qlnckhapp.R;
import com.example.qlnckhapp.adapter.SinhVienTGAdapter;
import com.example.qlnckhapp.dao.Dao_DeTai;
import com.example.qlnckhapp.dao.Dao_SinhVien;
import com.example.qlnckhapp.dao.Dao_SinhVienTG;
import com.example.qlnckhapp.model.DeTai;
import com.example.qlnckhapp.model.SinhVienTG;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChinhSuaDeTaiActivity extends AppCompatActivity {
    Spinner soLuongSv,loaidt,loailv,loaisp;
    LinearLayout sv1,sv2,sv3,sv4,sv5;
    EditText tendt,mota,muctieu,giaovien,phuongphap,kinhphi;
    EditText masv1,masv2,masv3,masv4,masv5;
    EditText tensv1,tensv2,tensv3,tensv4,tensv5;
    Button btnSave;
    ImageButton btnReturn,btn_check;
    int idDT;
    DeTai detai;
    SinhVienTGAdapter svTgAdapter;
    ArrayList<SinhVienTG> list;
    ArrayAdapter spinnerAdapter1,spinnerAdapter2,spinnerAdapter3;
    int n1,n2,n3,n4,n5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chinh_sua_de_tai);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Xử lý drop down số lượng sinh viên tham gia
        soLuongSv = (Spinner) findViewById(R.id.menu_soluong);
        sv1 = (LinearLayout) findViewById(R.id.layout_sv1);
        sv2 = (LinearLayout) findViewById(R.id.layout_sv2);
        sv3 = (LinearLayout) findViewById(R.id.layout_sv3);
        sv4 = (LinearLayout) findViewById(R.id.layout_sv4);
        sv5 = (LinearLayout) findViewById(R.id.layout_sv5);

        sv3.setVisibility(View.GONE);
        sv4.setVisibility(View.GONE);
        sv5.setVisibility(View.GONE);

        List<Integer> list = new ArrayList<>();

        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        ArrayAdapter spinnerAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, list);

        soLuongSv.setAdapter(spinnerAdapter);

        soLuongSv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if(soLuongSv.getSelectedItem().toString().equals("2"))
                {
                    sv1.setVisibility(View.VISIBLE);
                    sv2.setVisibility(View.VISIBLE);
                    sv3.setVisibility(View.GONE);
                    sv4.setVisibility(View.GONE);
                    sv5.setVisibility(View.GONE);
                }
                if(soLuongSv.getSelectedItem().toString().equals("3"))
                {
                    sv1.setVisibility(View.VISIBLE);
                    sv2.setVisibility(View.VISIBLE);
                    sv3.setVisibility(View.VISIBLE);
                    sv4.setVisibility(View.GONE);
                    sv5.setVisibility(View.GONE);
                }
                if(soLuongSv.getSelectedItem().toString().equals("4"))
                {
                    sv1.setVisibility(View.VISIBLE);
                    sv2.setVisibility(View.VISIBLE);
                    sv3.setVisibility(View.VISIBLE);
                    sv4.setVisibility(View.VISIBLE);
                    sv5.setVisibility(View.GONE);
                }
                if(soLuongSv.getSelectedItem().toString().equals("5"))
                {
                    sv1.setVisibility(View.VISIBLE);
                    sv2.setVisibility(View.VISIBLE);
                    sv3.setVisibility(View.VISIBLE);
                    sv4.setVisibility(View.VISIBLE);
                    sv5.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //quay lại
        btnReturn = (ImageButton) findViewById(R.id.btn_return);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //bắt đầu tại đây
        tendt = (EditText) findViewById(R.id.ed_tendetai);
        mota = (EditText) findViewById(R.id.ed_mota);
        muctieu = (EditText) findViewById(R.id.ed_muctieu);
        giaovien = (EditText) findViewById(R.id.ed_giaovien);
        loaidt = (Spinner) findViewById(R.id.menu_loaidetai);
        loailv = (Spinner) findViewById(R.id.menu_linhvuc);
        phuongphap = (EditText) findViewById(R.id.ed_phuongphap);
        loaisp = (Spinner) findViewById(R.id.menu_loaisanpham);
        kinhphi = (EditText) findViewById(R.id.ed_kinhphi);
        //
        // loand loaidetai
        Dao_DeTai daoDeTai = new Dao_DeTai(this);
        List<String> list1 = new ArrayList<>();
        list1 = daoDeTai.getAllldt();

        spinnerAdapter1 = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, list1);
        loaidt.setAdapter(spinnerAdapter1);
        //loand linhvuc
        List<String> list2 = new ArrayList<>();
        list2 = daoDeTai.getAllLV();

        spinnerAdapter2 = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, list2);
        loailv.setAdapter(spinnerAdapter2);
        //load sanpham
        List<String> list3 = new ArrayList<>();
        list3 = daoDeTai.getAlllsp();

        spinnerAdapter3 = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, list3);
        loaisp.setAdapter(spinnerAdapter3);

        //sinhvien
        masv1 = (EditText) findViewById(R.id.tv_masv1);
        masv2 = (EditText) findViewById(R.id.tv_masv2);
        masv3 = (EditText) findViewById(R.id.tv_masv3);
        masv4 = (EditText) findViewById(R.id.tv_masv4);
        masv5 = (EditText) findViewById(R.id.tv_masv5);
        tensv1 = (EditText) findViewById(R.id.tv_tensv1);
        tensv2 = (EditText) findViewById(R.id.tv_tensv2);
        tensv3 = (EditText) findViewById(R.id.tv_tensv3);
        tensv4 = (EditText) findViewById(R.id.tv_tensv4);
        tensv5 = (EditText) findViewById(R.id.tv_tensv5);


        //lấy id
        Intent intent = getIntent();
        idDT = intent.getIntExtra("idDT",0);
        getDeTai(detai);
        //gọi hàm load de tai
        loadDetai(detai);
        //lấy mã sv đăng nhập
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        String id = sharedPreferences.getString("masv","");
        //gọi hàm cập nhật
        Dao_SinhVien daoSV = new Dao_SinhVien(this);
        btnSave = (Button) findViewById(R.id.btn_chinhsua);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(kiemtra())
                {
                    chinhsua(detai);
                    Intent intent1 = new Intent(ChinhSuaDeTaiActivity.this,DeTaiCuaBanActivity.class);
                    intent1.putExtra("idDT",daoSV.findIdSVBymaSV(id));
                    startActivity(intent1);
                    finish();
                }

                else
                    return;
            }
        });
        btn_check = (ImageButton) findViewById(R.id.btn_check);
        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(kiemtra())
                {
                    chinhsua(detai);
                    Intent intent1 = new Intent(ChinhSuaDeTaiActivity.this,DeTaiCuaBanActivity.class);
                    intent1.putExtra("idDT",daoSV.findIdSVBymaSV(id));
                    startActivity(intent1);
                    finish();
                }

                else
                    return;
            }
        });
    }

    private void loadDetai(DeTai deTai) {
        Dao_DeTai daoDeTai = new Dao_DeTai(this);
        int ldt = deTai.getLoaiDT();
        String tenldt = daoDeTai.getLoaiDeTaiById(ldt);
        int lv = deTai.getLoaiDT();
        String tenlv = daoDeTai.getLoaiLinhVucById(lv);
        int sp = deTai.getLoaiDT();
        String tensp = daoDeTai.getLoaiSanPhamById(sp);

        tendt.setText(deTai.getTenDT());
        mota.setText(deTai.getMoTa());
        muctieu.setText(deTai.getMucTieu());
        giaovien.setText(deTai.getGiaoVienHD());
        loaidt.setSelection(spinnerAdapter1.getPosition(tenldt));
        loailv.setSelection(spinnerAdapter1.getPosition(tenlv));
        phuongphap.setText(deTai.getPhuongphapNC());
        loaisp.setSelection(spinnerAdapter1.getPosition(tensp));
        kinhphi.setText(deTai.getKinhphiDK());


        //
        masv1.setText(list.get(0).getMaSV());
        tensv1.setText(list.get(0).getTenSV());
        masv2.setText(list.get(1).getMaSV());
        tensv2.setText(list.get(1).getTenSV());
        Integer sl = Integer.parseInt(soLuongSv.getSelectedItem().toString());
        if (sl >= 3 )
        {
            masv3.setText(list.get(2).getMaSV());
            tensv3.setText(list.get(2).getTenSV());

            if(sl >= 4)
            {
                masv5.setText(list.get(3).getMaSV());
                tensv5.setText(list.get(3).getTenSV());
                if (sl>=5)
                {
                    masv5.setText(list.get(4).getMaSV());
                    tensv5.setText(list.get(4).getTenSV());
                }
            }
        }
    }
    void getDeTai(DeTai deTai)
    {
        Dao_DeTai daoDT = new Dao_DeTai(this);
        detai = new DeTai();
        detai = daoDT.getDeTaiById(idDT);

        Dao_SinhVienTG dao_SVTG = new Dao_SinhVienTG(this);
        list = new ArrayList<>();
        list = dao_SVTG.getAllSinhVienSub(idDT);
    }
    private void chinhsua(DeTai detai) {
        DeTai dt = new DeTai();
        Dao_DeTai daoDT = new Dao_DeTai(this);

        //gán giá trị vào đối tượng
        dt.setIdDT(detai.getIdDT());
        dt.setTenDT(tendt.getText().toString());
        dt.setMoTa(mota.getText().toString());
        dt.setMucTieu(muctieu.getText().toString());
        dt.setSoluongTG(Integer.parseInt(soLuongSv.getSelectedItem().toString()));
        dt.setGiaoVienHD(giaovien.getText().toString());
        dt.setLoaiDT(daoDT.getIDLDT(loaidt.getSelectedItem().toString()));
        dt.setIdLinhVuc(daoDT.getIDLV(loailv.getSelectedItem().toString()));
        dt.setPhuongphapNC(phuongphap.getText().toString());
        dt.setLoaiSP(daoDT.getIDSP(loaisp.getSelectedItem().toString()));
        dt.setKinhphiDK(kinhphi.getText().toString());
        dt.setIdSVDK(detai.getIdSVDK());
        dt.setNgayDK(detai.getNgayDK());
        if(daoDT.updateDeTai(dt)){
            capnhatsvtg(detai.getIdDT());
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Cập nhật đề tài thành công", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Lỗi ", Toast.LENGTH_LONG).show();
        }
    }

    private void capnhatsvtg(int id) {
        n1 = list.get(0).getIdNTG();
        n2 = list.get(1).getIdNTG();

        SinhVienTG sv1 = new SinhVienTG(n1,id,masv1.getText().toString(),tensv1.getText().toString());
        SinhVienTG sv2 = new SinhVienTG(n2,id,masv2.getText().toString(),tensv2.getText().toString());

        Dao_SinhVienTG daoSVTG = new Dao_SinhVienTG(this);
        daoSVTG.updateSVTG(sv1);
        daoSVTG.updateSVTG(sv2);
        Integer sl = Integer.parseInt(soLuongSv.getSelectedItem().toString());
        if (sl >= 3 )
        {
            n3 = list.get(3).getIdNTG();
            SinhVienTG sv3 = new SinhVienTG(n3,id,masv3.getText().toString(),tensv3.getText().toString());
            daoSVTG.updateSVTG(sv3);
            if(sl >= 4)
            {
                n4 = list.get(3).getIdNTG();
                SinhVienTG sv4 = new SinhVienTG(n4,id,masv4.getText().toString(),tensv4.getText().toString());
                daoSVTG.updateSVTG(sv4);
                if (sl>=5)
                {
                    n5 = list.get(4).getIdNTG();
                    SinhVienTG sv5 = new SinhVienTG(n5,id,masv5.getText().toString(),tensv5.getText().toString());
                    daoSVTG.updateSVTG(sv5);
                }
            }
        }
    }

    private boolean kiemtra() {
        if(TextUtils.isEmpty(tendt.getText())) {
            tendt.setError("Không được để trống tên đề tài");
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Không được để trống tên đề tài", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(mota.getText())) {
            mota.setError("Không được để trống mô tả");
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Không được để trống mô tả", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(giaovien.getText())) {
            giaovien.setError("Không được để trống giáo viên hướng dẫn");
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Không được để trống giáo viên hướng dẫn", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(phuongphap.getText())) {
            phuongphap.setError("Không được để trống phương pháp");
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Không được để trống phương pháp", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(kinhphi.getText())) {
            kinhphi.setError("Không được để trống kinh phí");
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Không được để trống kinh phí", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(masv1.getText()) || TextUtils.isEmpty(tensv1.getText()))
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Sinh viên tham gia 1 còn trống", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(masv2.getText()) || TextUtils.isEmpty(tensv2.getText()))
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Sinh viên tham gia 2 còn trống", Toast.LENGTH_LONG).show();
            return false;
        }
        int n = Integer.parseInt(soLuongSv.getSelectedItem().toString());
        if (n >= 3 && (TextUtils.isEmpty(masv3.getText()) || TextUtils.isEmpty(tensv3.getText())))
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Sinh viên tham gia 3 còn trống", Toast.LENGTH_LONG).show();
            return false;
        }
        if (n >= 4 && (TextUtils.isEmpty(masv4.getText()) || TextUtils.isEmpty(tensv4.getText())))
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Sinh viên tham gia 4 còn trống", Toast.LENGTH_LONG).show();
            return false;
        }
        if (n >= 5 && (TextUtils.isEmpty(masv5.getText()) || TextUtils.isEmpty(tensv5.getText())))
        {
            Toast.makeText(ChinhSuaDeTaiActivity.this, "Sinh viên tham gia 5 còn trống", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}